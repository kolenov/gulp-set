const gulp = require('gulp');
const browserSync = require('browser-sync').create();
// const exec = require('child_process').exec;
const config = require('../config');

gulp.task('reload', function() {
  return browserSync.reload();
});

gulp.task('connect', function() {
  browserSync.init({
    proxy: {
      target: '0.0.0.0:3000',
      proxyReq: [
        function(proxyReq) {
          proxyReq.setHeader('X-Forwarded-Host', 'localhost:3001');
        },
      ],
    },
    port: 3001,
    open: true,
    ui: {
      port: 3002,
    },
  });
});

gulp.task('serve', ['connect']);
