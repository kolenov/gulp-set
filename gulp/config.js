module.exports = {
  src: {
    root: './src/',
    scss: './src/stylesheets/',
    js: './src/javascripts/',
    img: './src/images/',
    helpers: './gulp/helpers/',
  },
  dest: {
    root: './app/',
    css: './app/assets/stylesheets/',
    img: './app/assets/images/',
    fonts: './app/assets/fonts/'
  },
  watch: {
    view: './app/views/**/*.slim',
    scss: './app/assets/stylesheets/**/*',
    js: './app/assets/javascripts/**/*',
    img: './app/assets/images/**/*',
  },
};
